#include "utils.h"
/*
input: arr of int, size of arr
output: none
change the order by reversing it using stack functions
*/
void reverse(int* nums, unsigned int size)
{
	int i;
	stack* head;
	initStack(&head);
	head->nodeValue = nums[0]; //set first value (head)
	head->next = NULL;
	for (i = 1; i < size; i++)
	{
		push(&head, nums[i]); //add the other values
	}


	for (i = 0; i < size - 1; i++)
	{
		nums[i] = pop(&head); //get the other values
	}
	nums[size - 1] = head->nodeValue; //add the last value (head)

	
}
/*
input: none
output: arr of int

get 10 numbers from user, order them reversly on array and return this array.
*/
int* reverse10()
{
	int i;//for the for loop
	int* arrNums = new int[10];

	for (i = 9; i >= 0; i--)
	{
		std::cout << "Enter arr[" << (9 - i) << "]: "; // no flush needed
		std::cin >> arrNums[i];
	}

	for (int i = 0; i < 10; i++)
	{
		std::cout << arrNums[i] << ", ";
	}

	return arrNums;
}