#pragma once
#include "linkedList.h"
/* a positive-integer value stack, with no size limit */
typedef struct stack
{
	int nodeValue;
	stack* next;
} stack;

//I tried not to change the function signs but I had to. I didn't success to do it without pointer to pointer (because it made a memory problem when it was only 'stack* s')
void push(stack** s, unsigned int element); 
int pop(stack** s); // Return -1 if stack is empty

void initStack(stack** s);
void cleanStack(stack** s);
