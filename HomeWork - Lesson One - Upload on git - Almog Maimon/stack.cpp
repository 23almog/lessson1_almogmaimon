#include "stack.h"
/*
input: head of stack, a value
output: none
it adds a node to the stack by value
*/
void push(stack** s, unsigned int element)
{
	stack* temp = *s;
	stack* newHead = new stack;
	newHead->nodeValue = element;
	newHead->next = temp;
	*s = newHead;
}
/*
input: head of stack
output: deleted node value (-1 of empty)
*/
int pop(stack** s)
{
	int toReturn ;
	if ((*s)->next)
	{
		toReturn = (*s)->nodeValue;
		stack* temp = *s;
		*s = (*s)->next;
		delete temp;
	}
	else
	{
		toReturn = -1;
	}
	return toReturn;
}
/*
input: head of stack
output: none
it sets a place in memory for the head of the stack;
*/
void initStack(stack** s)
{
	*s = new stack;
}
/*
input: head of linked list
output: none
it cleans the whole stack from memory
*/
void cleanStack(stack** s)
{
	stack* nextOfTemp = NULL; //in linkedList, we need to delete the 'next' first and then the node itseld, otherwise 'next' node will be unaccessable
	stack* temp = *s;
	
	while (!temp)
	{
		nextOfTemp = temp->next; //set the next of temp to be the next node of 'temp'
		delete temp; //we can now delete the temp itself (we got the 'next' node)
		temp = nextOfTemp; //moving one node forward
	}



}