#pragma once

#include <iostream>


typedef struct linkedList
{
	int nodeValue;
	linkedList* next;
} linkedList;

void addNode(linkedList** head, unsigned int value);
int delNode(linkedList** head);
