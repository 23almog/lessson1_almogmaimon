#include "linkedList.h"
/*
input: head of linked list, a value
output: none
it adds a node to the linked list by value
*/
void addNode(linkedList** head, unsigned int value)
{
	linkedList* temp = *head;
	linkedList* newHead = new linkedList;
	newHead->nodeValue = value;
	newHead->next = temp;
	*head = newHead;
}
/*
input: head of linked list
output: deleted node value (-1 of empty)
*/
int delNode(linkedList** head)
{
	int toReturn = (*head)->nodeValue;
	if ((*head)->next)
	{
		linkedList* temp = *head;
		*head = (*head)->next;
		delete temp;
	}
	else
	{
		toReturn = -1;
	}
	return toReturn;
}