#include "queue.h"
/*
initializes the queue
input: the queue, size of queue
output: none
creates the queue memory
*/
void initQueue(queue* q, unsigned int size)
{
	q->elements = new int[size];
	q->maxSize = size;
	q->count = 0;
}

/*
input: the queue
output: none
clean the queue from memory
*/
void cleanQueue(queue* q)
{
	delete[] q->elements;
}
/*
input: the queue, a value
output: none
adds a number to the queue
*/
void enqueue(queue* q, unsigned int newValue)
{ 
	if (q->count < q->maxSize) //there is empty memory for the value
	{
		q->elements[q->count++] = newValue;
	}
	else //there is not empty memory for the value
	{
		std::cout << "There is no empty place to add to the stack" << std::endl;
	}
}

/*
input: the queue
output: the value deleted in the queue
*/
int dequeue(queue* q)
{
	int toReturn; //value to return to the user
	int i; //for the for loop
	if (!q->count) //no values
	{
		toReturn = -1;
	}
	else if (q->count == 1) //only one value
	{
		toReturn = q->elements[0];
		q->count = 0;
	}
	else //there is 2 or more numbers in the stack
	{
		toReturn = q->elements[0];
		for (i = 0; i < q->count; i++)
		{
			q->elements[i] = q->elements[i + 1]; //moving all values 'upper'
		}
		q->count--;
	}

	return toReturn; //return the value needed to be returned
}